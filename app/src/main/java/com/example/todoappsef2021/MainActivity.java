package com.example.todoappsef2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button bt_login;
    ImageButton bt_createDb;
    EditText et_username, et_password;
    DbManager dbManager;
    SQLiteDatabase db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbManager = new DbManager(getApplicationContext());
        db = dbManager.open();
        dbManager.addUsers(db);
        // initialize buttons
        bt_login = findViewById(R.id.login);

        // initialize edit texts
        et_username = findViewById(R.id.username);
        et_password = findViewById(R.id.password);

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = et_username.getText().toString();
                String password = et_password.getText().toString();

                if (dbManager.validateUser(username, password, db)){
                    Toast.makeText(getApplicationContext(), "You are in", Toast.LENGTH_LONG).show();
                    int user_id = dbManager.getUserId(username, db);
                    Intent intent  = new Intent(MainActivity.this, UserProfileActivity.class);
                    intent.putExtra("USER_ID", user_id);
                    intent.putExtra("username", username);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "You are not in. Wrong username or password!", Toast.LENGTH_LONG).show();

                }
            }
        });

    }
}

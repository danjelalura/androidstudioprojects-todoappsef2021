package com.example.todoappsef2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserProfileActivity extends AppCompatActivity implements SensorEventListener {
    Button bt_todos, bt_projects;
    TextView username, tv_temp;
    DbManager dbManager;
    SQLiteDatabase db;
    SensorManager sensorManager;
    Sensor temperature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        dbManager = new DbManager(getApplicationContext());
        db = dbManager.open();

        bt_todos = findViewById(R.id.todosButton);
        bt_projects = findViewById(R.id.projectsButton);

        username = findViewById(R.id.profile_username);
        String username_value = getIntent().getStringExtra("username");
        username.setText(username_value);

        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        temperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        tv_temp = findViewById(R.id.temp_text);


        bt_todos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserProfileActivity.this, TodoActivity.class);
                int user_id = getIntent().getIntExtra("USER_ID", 0);
                intent.putExtra("USER_ID", user_id);
                startActivity(intent);
            }
        });

        bt_projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Go to Projects Activity
                Intent intent = new Intent(UserProfileActivity.this, ProjectsActivity.class);
                int user_id = getIntent().getIntExtra("USER_ID", 0);
                intent.putExtra("USER_ID", user_id);
                startActivity(intent);
            }
        });
    }



    public void onResume(){
        super.onResume();
        sensorManager.registerListener(this, temperature, sensorManager.SENSOR_DELAY_NORMAL);
    }
    public void onPause(){
        super.onPause();
    }
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        double mytemp = sensorEvent.values[0];

        if (mytemp < 40){
            tv_temp.setText("It's a great day to get work done!");
        }
        else {
            tv_temp.setText("I think you need some rest!");

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}

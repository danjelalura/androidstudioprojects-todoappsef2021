package com.example.todoappsef2021;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;

import java.util.ArrayList;
import java.util.List;



public class ProjectProgressActivity extends AppCompatActivity {
    TextView title;
    AnyChartView anyChartView;
    String[] tasks = {"COMPLETED", "PENDING"};
    int[] quantities = new int[tasks.length];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_progress);
        int total = Integer.parseInt(getIntent().getExtras().get("TOTAL_TASKS").toString());
        int completed = Integer.parseInt(getIntent().getExtras().get("COMPLETED_TASKS").toString());
        int pending = Integer.parseInt(getIntent().getExtras().get("PENDING_TASKS").toString());
        String project_title = getIntent().getExtras().get("PROJECT_TITLE").toString();

        title = (TextView)findViewById(R.id.prog_title);
        title.setText(project_title);

        quantities[0] = completed;
        quantities[1] = pending;


        anyChartView = findViewById(R.id.any_chart_view);
        setupChart();


    }

    public void setupChart(){
        List<DataEntry> dataEntryList = new ArrayList<>();
        Pie pie = AnyChart.pie();

        for (int i = 0; i < tasks.length; i++){
            dataEntryList.add(new ValueDataEntry(tasks[i], quantities[i]));
        }

        pie.data(dataEntryList);
        anyChartView.setChart(pie);
    }
}
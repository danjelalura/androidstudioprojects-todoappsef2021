package com.example.todoappsef2021;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AddProjectActivity extends AppCompatActivity {
    EditText project_title, completed_tasks, pending_tasks;
    DbManager dbManager;
    Button add_project;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        project_title = findViewById(R.id.add_project_title);
        completed_tasks = findViewById(R.id.add_project_completed);
        pending_tasks = findViewById(R.id.add_project_pending);
        add_project = findViewById(R.id.add_project_bt);
        dbManager = new DbManager(getApplicationContext());
        user_id = getIntent().getIntExtra("USER_ID", 0);

        add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String p_title = project_title.getText().toString();
                    int p_completed = Integer.parseInt(completed_tasks.getText().toString());
                    int p_pending = Integer.parseInt(pending_tasks.getText().toString());
                    Project project = new Project(0, p_title, user_id,p_completed+p_pending, p_pending, p_completed, R.drawable.ic_launcher_background, "NOT URGENT");

                    dbManager.addProject(project);

                    Intent intent = new Intent(AddProjectActivity.this, ProjectsActivity.class);
                    intent.putExtra("USER_ID", user_id);
                    startActivity(intent);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Please fill out the required fields!", Toast.LENGTH_LONG).show();
                }

            }

        });
    }
}
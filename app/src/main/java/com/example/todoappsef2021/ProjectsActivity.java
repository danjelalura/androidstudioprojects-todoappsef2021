package com.example.todoappsef2021;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageButton;
import java.util.ArrayList;

public class ProjectsActivity extends AppCompatActivity {
    static int user_id;
    ArrayList<Project> allProjects;
    ImageButton add_project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        final DbManager DBManager = new DbManager(getApplicationContext());
        SQLiteDatabase db = DBManager.open();

        user_id = getIntent().getIntExtra("USER_ID", 0);
        allProjects = new ArrayList<>();
        allProjects = DBManager.readProjectData(db,getApplicationContext(), user_id);
        add_project = findViewById(R.id.bt_addProject);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        Recycler_View_Adapter adapter = new Recycler_View_Adapter(allProjects, getApplication());
        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplication(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(ProjectsActivity.this, ProjectProgressActivity.class);
                intent.putExtra("TOTAL_TASKS", allProjects.get(position).totalTasks);
                intent.putExtra("COMPLETED_TASKS", allProjects.get(position).completedTasks);
                intent.putExtra("PENDING_TASKS", allProjects.get(position).pendingTasks);
                intent.putExtra("PROJECT_TITLE", allProjects.get(position).title);
                // intent.putExtra("PROJECT_ID", position);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }}));

        add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProjectsActivity.this, AddProjectActivity.class);
                intent.putExtra("USER_ID", user_id);
                startActivity(intent);
            }
        });

    }


}
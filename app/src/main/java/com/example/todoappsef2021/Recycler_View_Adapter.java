package com.example.todoappsef2021;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Recycler_View_Adapter extends RecyclerView.Adapter<Recycler_View_Adapter.View_Holder>{
    ArrayList<Project> list;
    Context context;

    public Recycler_View_Adapter(ArrayList<Project> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        View_Holder holder = new View_Holder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final View_Holder holder, final int position) {
        holder.title.setText(list.get(position).title);
        holder.status.setText(list.get(position).status);
        holder.imageView.setImageResource(list.get(position).imageId);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




    public void insert(int position, Project data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(Project data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

    public Context getContext() {
        return context;
    }




    public class View_Holder extends RecyclerView.ViewHolder{

        CardView cv;
        TextView title;
        TextView status;
        ImageView imageView;

        View_Holder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cardView);
            title = (TextView) itemView.findViewById(R.id.projectTitle);
            status = (TextView) itemView.findViewById(R.id.projectStatus);
            imageView = (ImageView) itemView.findViewById(R.id.projectImage);
        }



    }

}

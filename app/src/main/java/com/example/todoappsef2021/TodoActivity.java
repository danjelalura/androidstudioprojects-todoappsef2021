package com.example.todoappsef2021;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class TodoActivity extends AppCompatActivity {
    ImageButton bt_addTodo, bt_deleteAll;
    ListView lv_todo;
    static int user_id;
    SimpleCursorAdapter simpleCursorAdapter;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        bt_addTodo = findViewById(R.id.bt_addTodo);
        bt_deleteAll = findViewById(R.id.delete_all);
        lv_todo = findViewById(R.id.lv_todo);

        final DbManager DBManager = new DbManager(getApplicationContext());
        db = DBManager.open();

        ArrayList<String> allTodos = new ArrayList<>();
        user_id = getIntent().getIntExtra("USER_ID", 0);
        simpleCursorAdapter= DBManager.readTodoData(db,getApplicationContext(), user_id);
        if(simpleCursorAdapter!=null) {
            lv_todo.setAdapter(simpleCursorAdapter);
        }
        else Toast.makeText(getApplicationContext(),"No Todos Available",Toast.LENGTH_LONG).show();

        bt_addTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TodoActivity.this, AddTodoActivity.class);
                intent.putExtra("USER_ID", user_id);
                startActivity(intent);
            }
        });

        bt_deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBManager.deleteAllTodos(user_id);
                finish();
                startActivity(getIntent());
            }
        });

        lv_todo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object item = lv_todo.getItemAtPosition(i);
                Cursor cursor = (Cursor) item;
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                String diff = DBManager.getDifferenceInDays(id, db);

                try{
                    if (Integer.parseInt(diff) > 0){
                        Toast.makeText(getApplicationContext(), "You have " + diff + " days to finish this task!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "It has passed a lot of time since you defined this task.", Toast.LENGTH_LONG).show();

                    }
                }catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(), "TODO doesn't exist", Toast.LENGTH_LONG).show();
                }



            }
        });

    }

}
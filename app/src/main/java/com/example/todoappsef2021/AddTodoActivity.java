package com.example.todoappsef2021;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class AddTodoActivity extends AppCompatActivity {
    EditText et_todoTitle, et_todoDescription;
    TextView tv_todoDate;
    Button bt_addNewTodo;
    int user_id;

    DatePickerDialog.OnDateSetListener todoDateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);

        tv_todoDate = findViewById(R.id.tv_todoDate);
        bt_addNewTodo = findViewById(R.id.bt_addNewTodo);
        et_todoTitle = findViewById(R.id.et_todoTitle);
        et_todoDescription = findViewById(R.id.et_todoDescription);
        user_id = getIntent().getIntExtra("USER_ID", 0);

        tv_todoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(AddTodoActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        todoDateListener,
                        year, month, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        todoDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month= monthOfYear+1;
                String fm=""+month;
                String fd=""+dayOfMonth;
                if(month<10){
                    fm ="0"+month;
                }
                if (dayOfMonth<10){
                    fd="0"+dayOfMonth;
                }
                String date= ""+year+"-"+fm+"-"+fd;
                tv_todoDate.setText(date);
            }
        };

        bt_addNewTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String title = et_todoTitle.getText().toString();
                    String description = et_todoDescription.getText().toString();
                    String dateString = tv_todoDate.getText().toString();

                    if (title.isEmpty() || description.isEmpty() || dateString.equals("Pick a date")){
                        throw new Exception();
                    }
                    else {
                        Todo todo = new Todo(title, description, dateString, 0, user_id);
                        DbManager DBManager = new DbManager(getApplicationContext());
                        DBManager.addTodo(todo, user_id);

                        Toast.makeText(getApplicationContext(), todo.toString(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(AddTodoActivity.this, TodoActivity.class);
                        intent.putExtra("USER_ID", user_id);
                        startActivity(intent);
                    }

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Please fill out the required fields!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
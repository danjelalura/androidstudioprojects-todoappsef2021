package com.example.todoappsef2021;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class TodoListAdapter extends SimpleCursorAdapter {

    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;

    public TodoListAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int i) {
        super(context, layout, c, from, to);
        this.layout = layout;
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.cr = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }



    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        super.bindView(view, context, cursor);
        final int row_id = cursor.getInt(cursor.getColumnIndex("_id"));
        final String todo_title = cursor.getString(cursor.getColumnIndex("TITLE"));
        final String todo_description = cursor.getString(cursor.getColumnIndex("DESCRIPTION"));
        final String todo_date = cursor.getString(cursor.getColumnIndex("TODO_DATE"));


        ImageButton delete_btn = view.findViewById(R.id.todo_delete);
        ImageButton edit_btn = view.findViewById(R.id.todo_edit);

        delete_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String id_to_delete = String.valueOf(row_id);
                DbManager DBManager = new DbManager(context);
                DBManager.deleteTodo(id_to_delete);
                cursor.requery();
                Toast.makeText(context, "TODO Deleted", Toast.LENGTH_LONG).show();
            }
        });

        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditTodoActivity.class);
                intent.putExtra("_id", TodoActivity.user_id);
                intent.putExtra("TITLE", todo_title);
                intent.putExtra("DESCRIPTION", todo_description);
                intent.putExtra("TODO_DATE", todo_date);
                context.startActivity(intent);
            }
        });



    }
}

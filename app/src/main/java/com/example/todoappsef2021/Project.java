package com.example.todoappsef2021;

public class Project {
    public int _id;
    public String title;
    public int user_id;
    public int totalTasks;
    public int pendingTasks;
    public int completedTasks;
    public int imageId;
    public String status;

    public Project(int _id, String title, int user_id, int totalTasks, int pendingTasks, int completedTasks, int imageId, String status) {
        this._id = _id;
        this.title = title;
        this.user_id = user_id;
        this.totalTasks = totalTasks;
        this.pendingTasks = pendingTasks;
        this.completedTasks = completedTasks;
        this.imageId = imageId;
        this.status = status;
    }
}
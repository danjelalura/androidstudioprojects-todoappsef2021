package com.example.todoappsef2021;

public class Todo {
    private String title;
    private String description;
    private String date;
    private int status;
    private int user_id;

    public Todo(String title, String description, String date, int status, int user_id) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = status;
        this.user_id = user_id;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.date + ": " + this.title;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
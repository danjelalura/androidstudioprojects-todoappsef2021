package com.example.todoappsef2021;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DbManager extends SQLiteOpenHelper {

    public static final String USERS_TABLE = "USERS_TABLE";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String TODO_TABLE = "TODO_TABLE";
    public static final String TITLE = "TITLE";
    public static final String TODO_DATE = "TODO_DATE";
    public static final String USER_ID = "USER_ID";
    public static final String PROJECTS = "PROJECTS";
    public static final String TOTAL_TASKS = "TOTAL_TASKS";
    public static final String COMPLETED_TASKS = "COMPLETED_TASKS";
    public static final String PENDING_TASKS = "PENDING_TASKS";
    public static final String STATUS = "STATUS";
    public static final String IMAGE = "IMAGE";
    public static final String DESCRIPTION = "DESCRIPTION";

    public DbManager(Context context) {
        super(context.getApplicationContext(), "finalproject.db", null, 1);
    }

    public SQLiteDatabase open(){
        SQLiteDatabase db=getWritableDatabase();
        return db;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createUserTable =
                "CREATE TABLE " + USERS_TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        USERNAME + " TEXT UNIQUE," +
                        PASSWORD + " TEXT UNIQUE) ";

        String createTodoTable =
                "CREATE TABLE " + TODO_TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        TITLE + " TEXT, " + DESCRIPTION + " TEXT, " +
                        TODO_DATE + " TEXT, " + STATUS + " INTEGER, " +
                        USER_ID + " INTEGER, " +
                        "FOREIGN KEY(" + USER_ID + ") REFERENCES USER(_id));";

        String createProjectsTable =
                "CREATE TABLE " + PROJECTS + "(_id, " + TITLE + " TEXT, " +
                        USER_ID + " INTEGER, " +
                        TOTAL_TASKS + " INTEGER, " +
                        COMPLETED_TASKS + " INTEGER, " +
                        PENDING_TASKS + " INTEGER, " +
                        STATUS + " TEXT, " +
                        IMAGE + " INTEGER," +
                        "FOREIGN KEY(" + USER_ID + ") REFERENCES USER(_id))";


        sqLiteDatabase.execSQL(createUserTable);
        sqLiteDatabase.execSQL(createTodoTable);
        sqLiteDatabase.execSQL(createProjectsTable);
        Log.v("database", "DB created successfuly");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS USERS_TABLE");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TODO_TABLE);
        onCreate(sqLiteDatabase);

    }

    public void addUsers(SQLiteDatabase db){
        User user1 = new User("cs", "cs");
        User user2 = new User("act", "act");

        ContentValues contentValues = new ContentValues();

        contentValues.put(USERNAME, user1.getUserName());
        contentValues.put(PASSWORD, user1.getPassword());
        db.insert(USERS_TABLE, null, contentValues);

        contentValues.put(USERNAME, user2.getUserName());
        contentValues.put(PASSWORD, user2.getPassword());
        db.insert(USERS_TABLE, null, contentValues);
    }

    public void addTodo(Todo todo, int user_id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, todo.getTitle());
        contentValues.put("DESCRIPTION", todo.getDescription());
        contentValues.put(TODO_DATE, todo.getDate());
        contentValues.put(STATUS, todo.getStatus());
        contentValues.put(USER_ID, user_id);

        db.insert(TODO_TABLE, null, contentValues);
    }


    public void addProject(Project project1){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, project1.title);
        contentValues.put(TOTAL_TASKS, project1.totalTasks);
        contentValues.put(COMPLETED_TASKS, project1.completedTasks);
        contentValues.put(PENDING_TASKS, project1.pendingTasks);
        contentValues.put(IMAGE, project1.imageId);
        contentValues.put(STATUS, project1.status);
        contentValues.put(USER_ID, project1.user_id);
        db.insert(PROJECTS, null, contentValues);
    }

    public SimpleCursorAdapter readTodoData(SQLiteDatabase sqLiteDatabase, Context context, int user_id){
        SimpleCursorAdapter simpleCursorAdapter=null;
        ArrayList<String> allTodos = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(TODO_TABLE, new String[]{"_id", TITLE, "DESCRIPTION", TODO_DATE, STATUS, USER_ID}, USER_ID + "= " + user_id, null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                allTodos.add(cursor.getString(cursor.getColumnIndex(TITLE)));
//or cursor.getString(1);
                cursor.moveToNext();
            }
            simpleCursorAdapter = new TodoListAdapter(context.getApplicationContext(), R.layout.todoadapterlayout, cursor, new String[]{TODO_DATE, TITLE,"_id"}, new int[]{R.id.todoDate,R.id.todoTitle}, 0);

        }  catch(Exception ex)
        {
            Log.v("sql", "Problem");

        }
        return simpleCursorAdapter;
    }

    public void deleteTodo(String _id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TODO_TABLE, "_id=?", new String[]{_id});
    }
    public void deleteProject(String _id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(PROJECTS, "_id=?", new String[]{_id});
    }

    public void deleteAllTodos(int user_id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TODO_TABLE, USER_ID + "= " + user_id, null);
    }


    // Validate user

    public boolean validateUser(String userName, String password, SQLiteDatabase sqLiteDatabase) {
        String counts = "SELECT COUNT(*) AS counter FROM USERS_TABLE WHERE " + USERNAME + " = " + "'" + userName + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(counts, null);

        if (cursor.moveToFirst()) {
            cursor = sqLiteDatabase.query("USERS_TABLE", new String[]{PASSWORD}, USERNAME + " = " + "'" + userName + "'", null, null, null, null);

            while (cursor.moveToNext()) {
                return password.equals(cursor.getString(cursor.getColumnIndex(PASSWORD)));
            }
        }
        return false;
    }

    public int getUserId(String userName, SQLiteDatabase sqLiteDatabase){
        Cursor cursor = sqLiteDatabase.query("USERS_TABLE", new String[]{"_id"}, USERNAME + " = " + "'" + userName + "'", null, null, null, null);
        while (cursor.moveToNext()){
            return cursor.getInt(cursor.getColumnIndex("_id"));
        }
        return 0;
    }

    public int getTodoId(String todoTitle, SQLiteDatabase sqLiteDatabase){
        Cursor cursor = sqLiteDatabase.query(TODO_TABLE, new String[]{"_id"}, TITLE + " = " + "'" + todoTitle + "'", null, null, null, null);
        while (cursor.moveToNext()){
            return cursor.getInt(cursor.getColumnIndex("_id"));
        }
        return 0;
    }

    public int getProjectId(String projectTitle, SQLiteDatabase sqLiteDatabase){
        Cursor cursor = sqLiteDatabase.query("PROJECT", new String[]{"_id"}, TITLE + " = " + "'" + projectTitle + "'", null, null, null, null);
        while (cursor.moveToNext()){
            return cursor.getInt(cursor.getColumnIndex("_id"));
        }
        return 0;
    }


    public void updateTodo(Todo todo, int user_id, int todo_id){
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, todo.getTitle());
        contentValues.put(DESCRIPTION, todo.getDescription());
        contentValues.put(TODO_DATE, todo.getDate());
        contentValues.put(USER_ID, todo.getUser_id());
        contentValues.put(STATUS, 0);

        SQLiteDatabase sqLiteDatabase=open();
        sqLiteDatabase.update(TODO_TABLE, contentValues, USER_ID + "= " + user_id + " AND _id= "+ todo_id,null);


    }

    public String getDifferenceInDays(int todoId, SQLiteDatabase sqLiteDatabase){

        Cursor cursor = sqLiteDatabase.query(TODO_TABLE, new String[]{"CAST(julianday(" + TODO_DATE + ") - julianday('now') AS INTEGER) AS date"}, "_id = " + todoId, null, null, null, null);
        while (cursor.moveToNext()){
            return cursor.getString(cursor.getColumnIndex("date"));
        }
        return "";
    }
    public ArrayList<Project> readProjectData(SQLiteDatabase sqLiteDatabase, Context context, int user_id){
        SimpleCursorAdapter simpleCursorAdapter=null;
        ArrayList<Project> allProjects = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(PROJECTS, new String[]{"_id", TITLE, TOTAL_TASKS, COMPLETED_TASKS, PENDING_TASKS, IMAGE, STATUS, USER_ID}, USER_ID + "= " + user_id, null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String title = cursor.getString(cursor.getColumnIndex(TITLE));
                int total_tasks = cursor.getInt(cursor.getColumnIndex(TOTAL_TASKS));
                int _id = cursor.getInt(cursor.getColumnIndex("_id"));
                int completed_tasks = cursor.getInt(cursor.getColumnIndex(COMPLETED_TASKS));
                int pending_tasks = cursor.getInt(cursor.getColumnIndex(PENDING_TASKS));
                int image = cursor.getInt(cursor.getColumnIndex(IMAGE));
                String status = cursor.getString(cursor.getColumnIndex(STATUS));
                int users_id = cursor.getInt(cursor.getColumnIndex(USER_ID));

                Project project = new Project(_id, title, users_id, total_tasks, pending_tasks, completed_tasks, image, status);
                allProjects.add(project);
                cursor.moveToNext();
            }
            simpleCursorAdapter = new TodoListAdapter(context.getApplicationContext(), R.layout.todoadapterlayout, cursor, new String[]{TITLE, STATUS, IMAGE,"_id"}, new int[]{R.id.projectTitle,R.id.projectStatus, R.id.projectImage}, 0);

        }  catch(Exception ex)
        {
            Log.v("sql", "Problem");

        }
        return allProjects;
    }

}

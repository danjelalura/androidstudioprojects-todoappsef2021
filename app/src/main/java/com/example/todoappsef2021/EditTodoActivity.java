package com.example.todoappsef2021;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class EditTodoActivity extends AppCompatActivity {
    EditText et_todoTitle, et_todoDescription;
    TextView tv_todoDate;
    Button bt_editTodo;
    int user_id;
    String todoTitle;
    DbManager dbManager;

    DatePickerDialog.OnDateSetListener todoDateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_todo);


        et_todoTitle = findViewById(R.id.et_editTitle);
        et_todoDescription = findViewById(R.id.et_editDescription);
        tv_todoDate = findViewById(R.id.tv_editDate);
        bt_editTodo = findViewById(R.id.bt_editaddNewTodo);

        todoTitle = getIntent().getExtras().getString("TITLE");
        et_todoTitle.setText(todoTitle);
        et_todoDescription.setText(getIntent().getExtras().getString("DESCRIPTION"));
        user_id = getIntent().getIntExtra("_id", 0);

        dbManager = new DbManager(getApplicationContext());

        tv_todoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(EditTodoActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        todoDateListener,
                        year, month, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        todoDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month= monthOfYear+1;
                String fm=""+month;
                String fd=""+dayOfMonth;
                if(month<10){
                    fm ="0"+month;
                }
                if (dayOfMonth<10){
                    fd="0"+dayOfMonth;
                }
                String date= ""+year+"-"+fm+"-"+fd;
                tv_todoDate.setText(date);
            }
        };

        bt_editTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String updatedTitle = et_todoTitle.getText().toString();
                    String updatedDescription = et_todoDescription.getText().toString();
                    String updatedDate = tv_todoDate.getText().toString();
                    Todo todo = new Todo(updatedTitle, updatedDescription, updatedDate, 0, user_id);
                    dbManager.updateTodo(todo, user_id, dbManager.getTodoId(todoTitle, dbManager.open()));

                    Toast.makeText(getApplicationContext(), "TODO Updated", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(EditTodoActivity.this, TodoActivity.class);
                    intent.putExtra("USER_ID", user_id);
                    Log.v("USER_ID", String.valueOf(user_id));
                    startActivity(intent);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Please fill out the required fields!", Toast.LENGTH_LONG).show();

                }

            }
        });
    }
}